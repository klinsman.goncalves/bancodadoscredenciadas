package br.com.samples.bancodadoscredenciadas;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

import br.com.samples.bancodadoscredenciadas.dao.CredenciadasDataBase;
import br.com.samples.bancodadoscredenciadas.model.Credenciadas;

public class MainActivity extends AppCompatActivity {

    String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new BancoAsyncTask(this).execute();
    }


    class BancoAsyncTask extends AsyncTask<Void, Void, Void>{

        Context context;

        public BancoAsyncTask(Context context){
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Credenciadas credenciada = new Credenciadas();
            credenciada.setNome_fantasia("Primeira credenciada");

            CredenciadasDataBase.getInstance(context).getDao().insert(credenciada);
            Log.d(TAG, "Inseriu (teoricamente)");

            List<Credenciadas> credenciadas = CredenciadasDataBase.getInstance(context).getDao().getAllCredenciadas();

            Log.d(TAG, "Mostrar lista");
            for (Credenciadas c : credenciadas){
                Log.d(TAG, "-->" + c.toString());
            }
            return null;
        }
    }
}
