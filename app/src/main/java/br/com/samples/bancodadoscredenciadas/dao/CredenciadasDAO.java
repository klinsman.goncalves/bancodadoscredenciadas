package br.com.samples.bancodadoscredenciadas.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import br.com.samples.bancodadoscredenciadas.model.Credenciadas;

@Dao
public interface CredenciadasDAO {

    @Query("SELECT * FROM credenciadas")
    List<Credenciadas> getAllCredenciadas();

    @Insert
    void insert (Credenciadas... credenciadas);

    @Delete
    void delete (Credenciadas... credenciadas);

    @Update
    void update (Credenciadas... credenciadas);
}
