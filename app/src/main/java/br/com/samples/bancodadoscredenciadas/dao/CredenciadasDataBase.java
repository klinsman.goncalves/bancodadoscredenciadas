package br.com.samples.bancodadoscredenciadas.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import br.com.samples.bancodadoscredenciadas.model.Credenciadas;

@Database(entities = {Credenciadas.class}, version = 1)
public abstract class CredenciadasDataBase extends RoomDatabase {
    private static final String DB_NAME = "credenciadas.db";
    private static volatile CredenciadasDataBase instance;

    public static CredenciadasDataBase getInstance(Context context){
        if(instance == null){
            instance = create(context);
        }
        return instance;
    }

    private static CredenciadasDataBase create(Context context){
        return Room.databaseBuilder(context, CredenciadasDataBase.class, DB_NAME).build();
    }

    public abstract CredenciadasDAO getDao();
}
